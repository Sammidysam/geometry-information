\documentclass[12pt,letterpaper]{article}

\usepackage{color}
\usepackage{enumitem}
\usepackage{amsmath}
\usepackage{siunitx}

\newcounter{Chapter}
\stepcounter{Chapter}

\newcommand{\abs}[1]{\lvert #1 \rvert}
%% The following two commands use TeX magic to take the succeeding single paragraph as an argument.
\def\corollary#1\par{\begin{enumerate}[label=\textcolor{red}{Corollary},leftmargin=*]\item #1\end{enumerate}}
\def\related#1\par{\begin{enumerate}[label=\textcolor{red}{Related Formulas},leftmargin=*]\item #1\end{enumerate}}

\newenvironment{additional}{\begin{enumerate}[label=(\arabic*),leftmargin=*,nosep]}{\end{enumerate}}
\newenvironment{corollaries}{\begin{enumerate}[label=\textcolor{red}{Corollary \arabic*},leftmargin=*]}{\end{enumerate}}
\newenvironment{theorems}[1]{\noindent\textcolor{blue}{\large #1}\begin{enumerate}[label=\arabic{Chapter}-\arabic*,leftmargin=*]}{\end{enumerate}\stepcounter{Chapter}}

\begin{document}

\centerline{\textcolor{red}{\LARGE Geometry Theorems}}

\bigskip
\begin{theorems}{Points, Lines, Planes, and Angles}
  
\item If two lines intersect, then they intersect in exactly one point.
  
\item Through a line and a point not in the line there is exactly one plane.
  
\item If two lines intersect, then exactly one plane contains the lines.
  
\end{theorems}

\begin{theorems}{Deductive Reasoning}
  
\item \textbf{(Midpoint Theorem)}
  
  If $M$ is the midpoint of $\overline{\mathit{AB}}$, then $\mathit{AM} = \frac{1}{2}\mathit{AB}$ and $\mathit{MB} = \frac{1}{2}\mathit{AB}$.
  
\item \textbf{(Angle Bisector Theorem)}
  
  If $\overrightarrow{\mathit{BX}}$ is the bisector of $\angle{\mathit{ABC}}$, then $m\angle{\mathit{ABX}} = \frac{1}{2}m\angle{\mathit{ABC}}$ and $m\angle{\mathit{XBC}} = \frac{1}{2}m\angle{\mathit{ABC}}$.
  
\item Vertical angles are congruent.
  
\item If two lines are perpendicular, then they form congruent adjacent angles.
  
\item If two lines form congruent adjacent angles, then the lines are perpendicular.
  
\item If the exterior sides of two adjacent acute angles are perpendicular, then the angles are complementary.
  
\item If two angles are supplements of congruent angles (or of the same angle), then the two angles are congruent.
  
\item If two angles are complements of congruent angles (or of the same angle), then the two angles are congruent.
  
\end{theorems}

\begin{theorems}{Parallel Lines and Planes}
  
\item If two parallel planes are cut by a third plane, then the lines of intersection are parallel.
  
\item If two parallel lines are cut by a transversal, then alternate interior angles are congruent.
  
\item If two parallel lines are cut by a transversal, then same-side interior angles are supplementary.
  
\item If a transversal is perpendicular to one of two parallel lines, then it is perpendicular to the other one also.
  
\item If two lines are cut by a transversal and alternate interior angles are congruent, then the lines are parallel.
  
\item If two lines are cut by a transversal and same-side interior angles are supplementary, then the lines are parallel.
  
\item In a plane, two lines perpendicular to the same line are parallel.
  
\item Through a point outside a line, there is exactly one line parallel to the given line.
  
\item Through a point outside a line, there is exactly one line perpendicular to the given line.
  
\item Two lines parallel to a third line are parallel to each other.
  
\item The sum of the measures of the angles of a triangle is 180.
  
  \begin{corollaries}
    
  \item If two angles of one triangle are congruent to two angles of another triangle, then the third angles are congruent.
    
  \item Each angle of a equiangular triangle has measure 60.
    
  \item In a triangle, there can be at most one right angle or obtuse angle.
    
  \item The acute angles of a right triangle are complementary.
    
  \end{corollaries}
  
\item The measure of an exterior angle of a triangle equals the sum of the measures of the two remote interior angles.
  
\item The sum of the measures of the angles of a convex polygon with $n$ sides is $(n - 2)180$.
  
\item The sum of the measures of the exterior angles of any convex polygon, one angle at each vertex, is 360.
  
\end{theorems}

\begin{theorems}{Congruent Triangles}
  
\item \textbf{(The Isosceles Triangle Theorem)}
  
  If two sides of a triangle are congruent, then the angles opposite those sides are congruent.
  
  \begin{corollaries}
    
  \item An equilateral triangle is also equiangular.
    
  \item An equilateral triangle has three $60^\circ$ angles.
    
  \item The bisector of the vertex angle of an isosceles triangle is perpendicular to the base at its midpoint.
    
  \end{corollaries}
  
\item If two angles of a triangle are congruent, then the sides opposite those angles are congruent.
  
  \corollary An equiangular triangle is also equilateral.
  
\item \textbf{(AAS Theorem)}
  
  If two angles and the non-included side of one triangle are congruent to the corresponding parts of another triangle, then the triangles are congruent.
  
\item \textbf{(HL Theorem)}
  
  If the hypotenuse and a leg of one right triangle are congruent to the corresponding parts of another right triangle, then the triangles are congruent.
  
\item If a point lies on the perpendicular bisector of a segment, then the point is equidistant from the endpoints of the segment.
  
\item If a point is equidistant from the endpoints of a segment, then the point lies on the perpendicular bisector of the segment.
  
\item If a point lies on the bisector of an angle, then the point is equidistant from the sides of the angle.
  
\item If a point is equidistant from the sides of an angle, then the point lies on the bisector of the angle.
  
\end{theorems}

\begin{theorems}{Quadrilaterals}
  
\item Opposite sides of a parallelogram are congruent.
  
\item Opposite angles of a parallelogram are congruent.
  
\item Diagonals of a parallelogram bisect each other.
  
\item If both pairs of opposite sides of a quadrilateral are congruent, then the quadrilateral is a parallelogram.
  
\item If one pair of opposite sides of a quadrilateral are both congruent and parallel, then the quadrilateral is a parallelogram.
  
\item If both pairs of opposite angles of a quadrilateral are congruent, then the quadrilateral is a parallelogram.
  
\item If the diagonals of a quadrilateral bisect each other, then the quadrilateral is a parallelogram.
  
\item If two lines are parallel, then all points on one line are equidistant from the other line.
  
\item If three parallel lines cut off congruent segments on one transversal, then they cut off congruent segments on every transversal.
  
\item A line that contains the midpoint of one side of a triangle and is parallel to another side passes through the midpoint of the third side.
  
\item The segment that joins the midpoints of two sides of a triangle
  
  \begin{additional}
    
  \item is parallel to the third side.
    
  \item is half as long as the third side.
    
  \end{additional}
  
\item The diagonals of a rectangle are congruent.
  
\item The diagonals of a rhombus are perpendicular.
  
\item Each diagonal of a rhombus bisects two angles of the rhombus.
  
\item The midpoint of the hypotenuse of a right triangle is equidistant from the three vertices.
  
\item If an angle of a parallelogram is a right angle, then the parallelogram is a rectangle.
  
\item If two consecutive sides of a parallelogram are congruent, then the parallelogram is a rhombus.
  
\item Base angles of an isosceles trapezoid are congruent.
  
\item The median of a trapezoid
  
  \begin{additional}
    
  \item is parallel to the bases.
    
  \item has a length equal to the average of the base lengths.
    
  \end{additional}
  
\end{theorems}

\begin{theorems}{Inequalities in Geometry}
  
\item \textbf{(The Exterior Angle Inequality Theorem)}
  
  The measure of an exterior angle of a triangle is greater than the measure of either remote interior angle.
  
\item If one side of a triangle is longer than a second side, then the angle opposite the first side is larger than the angle opposite the second side.
  
\item If one angle of a triangle is larger than a second angle, then the side opposite the first angle is longer than the side opposite the second angle.
  
  \begin{corollaries}
    
  \item The perpendicular segment from a point to a line is the shortest segment from the point to the line.
    
  \item The perpendicular segment from a point to a plane is the shortest segment from the point to the plane.
    
  \end{corollaries}
  
\item \textbf{(The Triangle Inequality)}
  
  The sum of the lengths of any two sides of a triangle is greater than the length of the third side.
  
\item \textbf{(SAS Inequality Theorem)}
  
  If two sides of one triangle are congruent to two sides of another triangle, but the included angle of the first triangle is larger than the included angle of the second, then the third side of the first triangle is longer than the third side of the second triangle.
  
\item \textbf{(SSS Inequality Theorem)}
  
  If two sides of one triangle are congruent to two sides of another triangle, but the third side of the first triangle is longer than the third side of the second, then the included angle of the first triangle is larger than the included angle of the second.
  
\end{theorems}

\begin{theorems}{Similar Polygons}
  
\item \textbf{(SAS Similarity Theorem)}
  
  If an angle of one triangle is congruent to an angle of another triangle and the sides including those angles are in proportion, then the triangles are similar.
  
\item \textbf{(SSS Similarity Theorem)}
  
  If the sides of two triangles are in proportion, then the triangles are similar.
  
\item \textbf{(Triangle Proportionality Theorem)}
  
  If a line parallel to one side of a triangle intersects the other two sides, then it divides those sides proportionally.
  
  \corollary If three parallel lines intersect two transversals, then they divide the transversals proportionally.
  
\item \textbf{(Triangle Angle-Bisector Theorem)}
  
  If a ray bisects an angle of a triangle, then it divides the opposite side into segments proportional to the other two sides.
  
\end{theorems}

\begin{theorems}{Right Triangles}
  
\item If the altitude is drawn to the hypotenuse of a right triangle, then the two triangles formed are similar to the original triangle and to each other.
  
  \begin{corollaries}
    
  \item When the altitude is drawn to the hypotenuse of a right triangle, the length of the altitude is the geometric mean between the segments of the hypotenuse.
    
  \item When the altitude is drawn to the hypotenuse of a right triangle, each leg is the geometric mean between the hypotenuse and the segment of the hypotenuse that is adjacent to that leg.
    
  \end{corollaries}
  
\item \textbf{(Pythagorean Theorem)}
  
  In a right triangle, the square of the hypotenuse is equal to the sum of the squares of the legs.
  
\item If the square of one side of a triangle is equal to the sum of the squares of the other two sides, then the triangle is a right triangle.
  
\item If the square of the longest side of a triangle is less than the sum of the squares of the other two sides, then the triangle is an acute triangle.
  
\item If the square of the longest side of a triangle is greater than the sum of the squares of the other two sides, then the triangle is an obtuse triangle.
  
\item \textbf{(\ang[detect-weight]{45}-\ang[detect-weight]{45}-\ang[detect-weight]{90} Theorem)}
  
  In a \ang{45}-\ang{45}-\ang{90} triangle, the hypotenuse is $\sqrt{2}$ times as long as a leg.
  
\item \textbf{(\ang[detect-weight]{30}-\ang[detect-weight]{60}-\ang[detect-weight]{90} Theorem)}
  
  In a \ang{30}-\ang{60}-\ang{90} triangle, the hypotenuse is twice as long as the shorter leg, and the longer leg is $\sqrt{3}$ times as long as the shorter leg.
  
\end{theorems}

\begin{theorems}{Circles}
  
\item If a line is tangent to a circle, then the line is perpendicular to the radius drawn to the point of tangency.
  
  \corollary Tangents to a circle from a point are congruent.
  
\item If a line in the plane of a circle is perpendicular to a radius at its outer endpoint, then the line is tangent to the circle.
  
\item In the same circle or in congruent circles, two minor arcs are congruent if and only if their central angles are congruent.
  
\item In the same circle or in congruent circles,
  
  \begin{additional}
    
  \item congruent arcs have congruent chords.
    
  \item congruent chords have congruent arcs.
    
  \end{additional}
  
\item A diameter that is perpendicular to a chord bisects the chord and its arc.
  
\item In the same circle or in congruent circles,
  
  \begin{additional}
    
  \item chords equally distant from the center (or centers) are congruent.
    
  \item congruent chords are equally distant from the center (or centers).
    
  \end{additional}
  
\item The measure of an inscribed angle is equal to half the measure of its intercepted arc.
  
  \begin{corollaries}
    
  \item If two inscribed angles intercept the same arc, then the angles are congruent.
    
  \item An angle inscribed in a semicircle is a right angle.
    
  \item If a quadrilateral is inscribed in a circle, then its opposite angles are supplementary.
    
  \end{corollaries}
  
\item The measure of an angle formed by a chord and a tangent is equal to half the measure of the intercepted arc.
  
\item The measure of an angle formed by two chords that intersect inside a circle is equal to half the sum of the measures of the intercepted arcs.
  
\item The measure of an angle formed by two secants, two tangents, or a secant and a tangent drawn from a point outside a circle is equal to half the difference of the measures of the intercepted arcs.
  
\item When two chords intersect inside a circle, the product of the segments of one chord equals the product of the segments of the other chord.
  
\item When two secant segments are drawn to a circle from an external point, the product of one secant segment and its external segment equals the product of the other secant segment and its external segment.
  
\item When a secant segment and a tangent segment are drawn to a circle from an external point, the product of the secant segment and its external segment is equal to the square of the tangent segment.
  
\end{theorems}

\begin{theorems}{Constructions and Loci}
  
\item The bisectors of the angles of a triangle intersect in a point that is equidistant from the three sides of the triangle.
  
\item The perpendicular bisectors of the sides of a triangle intersect in a point that is equidistant from the three vertices of the triangle.
  
\item The lines that contain the altitudes of a triangle intersect in a point.
  
\item The medians of a triangle intersect in a point that is two thirds of the distance from each vertex to the midpoint of the opposite side.
  
\end{theorems}

\begin{theorems}{Areas of Plane Figures}
  
\item The area of a rectangle equals the product of its base and height. ($A = bh$)
  
\item The area of a parallelogram equals the product of a base and the height to that base. ($A = bh$)
  
\item The area of a triangle equals half the product of a base and the height to the base. ($A = \frac{1}{2}bh$)
  
\item The area of a rhombus equals half the product of its diagonals. ($A = \frac{1}{2}d_1d_2$)
  
\item The area of a trapezoid equals half the product of the height and the sum of the bases. ($A = \frac{1}{2}h(b_1 + b_2)$)
  
\item The area of a regular polygon is equal to half the product of the apothem and the perimeter. ($A = \frac{1}{2}ap$)
  
  \related In a circle: $C = 2\pi r = \pi d$ \hspace{1cm} $A = \pi r^2$
  
\item If the scale factor of two similar figures is $a:b$, then
  
  \begin{additional}

  \item the ratio of the perimeters is $a:b$.
    
  \item the ratio of the areas is $a^2:b^2$.
    
  \end{additional}
  
\end{theorems}

\begin{theorems}{Areas and Volumes of Solids}

\item The lateral area of a right prism equals the perimeter of a base times the height of the prism. ($\mathrm{L.A.} = ph$)
  
\item The volume of a right prism equals the area of a base times the height of the prism. ($V = Bh$)
  
\item The lateral area of a regular pyramid equals half the perimeter of the base times the slant height. ($\mathrm{L.A.} = \frac{1}{2}pl$)
  
\item The volume of a pyramid equals one third the area of the base times the height of the pyramid. ($V = \frac{1}{3}Bh$)
  
\item The lateral area of a cylinder equals the circumference of a base times the height of the cylinder. ($\mathrm{L.A.} = 2\pi rh$)
  
\item The volume of a cylinder equals the area of a base times the height of the cylinder. ($V = \pi r^2h$)
  
\item The lateral area of a cone equals half the circumference of the base times the slant height. ($\mathrm{L.A.} = \frac{1}{2}\cdot 2\pi r\cdot l$ or $\mathrm{L.A.} = \pi rl$)
  
\item The volume of a cone equals one third the area of the base times the height of the cone. ($V = \frac{1}{3}\pi r^2h$)
  
\item The area of a sphere equals $4\pi$ times the square of the radius. ($A = 4\pi r^2$)
  
\item The volume of a sphere equals $\frac{4}{3}\pi$ times the cube of the radius. ($V = \frac{4}{3}\pi r^3$)
  
\item If the scale factor of two similar solids is $a:b$, then

  \begin{additional}

  \item the ratio of corresponding perimeters is $a:b$.
    
  \item the ratio of the base areas, of the lateral areas, and of the total areas is $a^2:b^2$.

  \item the ratio of the volumes is $a^3:b^3$.
    
  \end{additional}
  
\end{theorems}

\begin{theorems}{Coordinate Geometry}

\item \textbf{(The Distance Formula)}
  
  The distance $d$ between points $(x_1, y_1)$ and $(x_2, y_2)$ is given by $d = \sqrt{(x_2 - x_1)^2 + (y_2 - y_1)^2}$.
  
\item An equation of the circle with center $(a, b)$ and radius $r$ is $(x - a)^2 + (y - b)^2 = r^2$.
  
\item Two nonvertical lines are parallel if and only if their slopes are equal.
  
\item Two nonvertical lines are perpendicular if and only if the product of their slopes is -1.
  
  \centerline{$m_1 \cdot m_2 = -1$, or $m_1 = -\dfrac{1}{m_2}$}
  
\item \textbf{(The Midpoint Formula)}
  
  The midpoint of the segment that joins points $(x_1, y_1)$ and $(x_2, y_2)$ is the point $\left(\dfrac{x_1 + x_2}{2}, \dfrac{y_1 + y_2}{2}\right)$.
  
\item \textbf{(Standard Form)}
  
  The graph of any equation that can be written in the form $Ax + By = C$, with $A$ and $B$ not both zero, is a line.
  
\item \textbf{(Slope-Intercept Form)}
  
  A line with the equation $y = mx + b$ has slope $m$ and y-intercept $b$.
  
\item \textbf{(Point-Slope Form)}
  
  An equation of the line that passes through the point $(x_1, y_1)$ and has slope $m$ is $y - y_1 = m(x - x_1)$.
  
\end{theorems}

\begin{theorems}{Transformations}
  
\item An isometry maps a triangle to a congruent triangle.
  
  \begin{corollaries}
    
  \item An isometry maps an angle to a congruent angle.

  \item An isometry maps a polygon to a polygon with the same area.
    
  \end{corollaries}

\item A reflection in a line is an isometry.

\item A translation is an isometry.
  
\item A rotation is an isometry.

\item A dilation maps any triangle to a similar triangle.
  
  \begin{corollaries}

  \item A dilation maps an angle to a congruent angle.
    
  \item A dilation $D_{O,k}$ maps any segment to a parallel segment $\abs{k}$ times as long.
    
  \item A dilation $D_{O,k}$ maps any polygon to a similar polygon whose area is $k^2$ times as large.

  \end{corollaries}

\item The composite of two isometries is an isometry.

\item A composite of reflections in two parallel lines is a translation.
  This translation glides all points through twice the distance from the first line of reflection to the second.

\item A composite of reflections in two intersecting lines is a rotation about the point of intersection of the two lines.
  The measure of the angle of rotation is twice the measure of the angle from the first line of reflection to the second.

  \corollary A composite of reflections in perpendicular lines is a half-turn about the point where the lines intersect.
  
\end{theorems}

\end{document}
